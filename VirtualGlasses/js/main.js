let dataGlasses = [
  {
    id: "G1",
    src: "./img/g1.jpg",
    virtualImg: "./img/v1.png",
    brand: "Armani Exchange",
    name: "Bamboo wood",
    color: "Brown",
    price: 150,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? ",
  },
  {
    id: "G2",
    src: "./img/g2.jpg",
    virtualImg: "./img/v2.png",
    brand: "Arnette",
    name: "American flag",
    color: "American flag",
    price: 150,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G3",
    src: "./img/g3.jpg",
    virtualImg: "./img/v3.png",
    brand: "Burberry",
    name: "Belt of Hippolyte",
    color: "Blue",
    price: 100,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G4",
    src: "./img/g4.jpg",
    virtualImg: "./img/v4.png",
    brand: "Coarch",
    name: "Cretan Bull",
    color: "Red",
    price: 100,
    description: "In assumenda earum eaque doloremque, tempore distinctio.",
  },
  {
    id: "G5",
    src: "./img/g5.jpg",
    virtualImg: "./img/v5.png",
    brand: "D&G",
    name: "JOYRIDE",
    color: "Gold",
    price: 180,
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?",
  },
  {
    id: "G6",
    src: "./img/g6.jpg",
    virtualImg: "./img/v6.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Blue, White",
    price: 120,
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit.",
  },
  {
    id: "G7",
    src: "./img/g7.jpg",
    virtualImg: "./img/v7.png",
    brand: "Ralph",
    name: "TORTOISE",
    color: "Black, Yellow",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam.",
  },
  {
    id: "G8",
    src: "./img/g8.jpg",
    virtualImg: "./img/v8.png",
    brand: "Polo",
    name: "NATTY ICE",
    color: "Red, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim.",
  },
  {
    id: "G9",
    src: "./img/g9.jpg",
    virtualImg: "./img/v9.png",
    brand: "Coarch",
    name: "MIDNIGHT VIXEN REMIX",
    color: "Blue, Black",
    price: 120,
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet.",
  },
];

let imgGlasses = "";
dataGlasses.forEach((item) => {
  imgGlasses += `<img class="col-4 mt-5" src="${item.src}" onclick="glassSelected('${item.id}')" alt="" />`;
});

let selectedGlass = "";

document.getElementById("vglassesList").innerHTML = imgGlasses;

function glassSelected(id) {
  let glass = dataGlasses[dataGlasses.findIndex((glass) => glass.id == id)];
  console.log(glass);
  selectedGlass = glass.virtualImg;
  document.getElementById(
    "avatar"
  ).innerHTML = `<img src="${selectedGlass}" alt="" />`;

  document.getElementById(
    "glassesInfo"
  ).innerHTML = `<h6>${glass.name} - ${glass.brand} (${glass.color})</h6>   
  <div class="vglass__info__price">
    <span>$${glass.price}</span>
  </div>
  <p>${glass.description}</p>`;

  document.getElementById("glassesInfo").classList.add("showVglasses__info");
}
window.glassSelected = glassSelected;

function removeGlasses(flag) {
  if (flag == true && selectedGlass != "") {
    document.getElementById(
      "avatar"
    ).innerHTML = `<img src="${selectedGlass}" alt="" />`;
    document.getElementById("glassesInfo").classList.add("showVglasses__info");
  } else {
    document.getElementById("avatar").innerHTML = "";
    document
      .getElementById("glassesInfo")
      .classList.remove("showVglasses__info");
  }
}
window.removeGlasses = removeGlasses;
